/*!*******************************************************************************************
 *  \file       performance_monitor_process.cpp
 *  \brief      PerformanceMonitor main file.
 *  \details    This file includes the PerformanceMonitor main declaration.
 *  \author     Enrique Ortiz
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/
#include "performance_monitor.h"

//! This main do not follow the usual pattern to execute a DroneProcess class, due to its class nature.
int main(int argc, char **argv)
{
  PerformanceMonitor pMonitor (argc, argv);    
}
